import React, { Component } from 'react';
import { connect } from 'react-redux'
import Client from './containers/client';
import Caregiver from './containers/caregiver';
import './App.css';
import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col } from 'reactstrap';
import classnames from 'classnames';

class App extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1',
      tempRequests: [],
      tempRequestsApplications: []
    };

    this.addRequest = this.addRequest.bind(this);
    this.registerApplication = this.registerApplication.bind(this);
  }

  // Toggle Modal: 'Add Entry'
  addRequest(content) {
    const newState_tempRequests = [...this.state.tempRequests];
    newState_tempRequests.push(content);
    this.setState({ tempRequests: newState_tempRequests} );
  }

  registerApplication(caregiverName, requestName) {
    const newState_tempRequestsApplications = [...this.state.tempRequestsApplications];
    newState_tempRequestsApplications.push({caregiverName: caregiverName, requestName: requestName});
    this.setState({ tempRequestsApplications: newState_tempRequestsApplications} );  
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    return (
      <div className="App">
        <div>
          <Nav tabs>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '1' })}
                onClick={() => { this.toggle('1'); }}
              >
                Client
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '2' })}
                onClick={() => { this.toggle('2'); }}
              >
                Caregiver
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="1" className="Client">
              <Row>
                <Col sm="6">
                  <h4>Welcome {this.props.user}</h4>
                </Col>
              </Row>
              <Row>
                <Col sm="6">
                  <Client addRequestBuffer={this.addRequest}/>
                </Col>
              </Row>
            </TabPane>
            <TabPane tabId="2" className="Caregiver">
              <Row>
                <Col sm="12">
                  <h4>Let's help someone, {this.props.user}</h4>
                </Col>
              </Row>
              <Row>
                <Col sm="12">
                  <Caregiver tempRequests={this.state.tempRequests} registerApplicationBuffer={this.registerApplication}/>
                </Col>
              </Row>   
            </TabPane>
          </TabContent>
        </div>      
      </div>
    );
  }
}


export const mapStateToProps = store => {
  return {
    user: store.user.context,
  }
}
export const mapDispatchToProps = dispatch => {
  return {

  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App)