export const requestForm = [
    {
        type: 'select',
        name: 'type',
        label: 'Kind of care',
        options: ['household', 'medical']
    },
    {
        type: 'date',
        name: 'startDate',
        label: 'Start'
    },
    {
        type: 'date',
        name: 'endDate',
        label: 'End'
    },
    {
        type: 'text',
        name: 'name',
        label: 'Patient',
        placeholder: 'Your name :)'
    },
    {
        type: 'textarea',
        name: 'info',
        label: 'More Info',
        placeholder: 'Observations...'
    },
    {
        type: 'select',
        name: 'status',
        label: 'Status',
        options: ['open', 'closed']
    }
];

export const requestApply = [
    {
        type: 'text',
        name: 'name',
        label: 'Caregiver',
        placeholder: 'Who will be helping!'
    },
]