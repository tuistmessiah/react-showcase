import React, { Component } from 'react';
// Reacstrap
import { Table } from 'reactstrap';
import { Row, Col } from 'reactstrap';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
// Components
import FormC from '../components/FormC';
import List from '../components/List';
// Constants
import { requestApply } from '../constants/formFormats';

class Caregiver extends Component {
    constructor(props) {
        super(props);

        this.state = {
            details: { entry: null, isToggled: false },
            appliedRequests: [],
            modal: false,
            modalOpened: null,
            requestApplyForm: requestApply
        }

        this.toggle = this.toggle.bind(this);
        this.showDetails = this.showDetails.bind(this);
        this.apply = this.apply.bind(this);
        this.submitApply = this.submitApply.bind(this);
    }

    // Toggle Modal: 'Add Entry'
    toggle(e, item) {
        this.setState({ modal: false }); 
    }

    showDetails(entry) {
        const newState_details = { entry: entry, isToggled: true}
        this.setState({ details: newState_details });
    }

    apply(e, status, item) {
        if(this.state.appliedRequests.find( name => name === item.name)) {
        } else {
            this.setState({ modal: true, requestName: item.name }); 
        }
    }

    submitApply(item) {
        this.props.registerApplicationBuffer(item.name, this.state.requestName);
        this.setState({ modal: false, requestName:  null });
    }

    render() {

        return ( 
            <div>
                <Row>
                    {/* List of Requests */}
                    <Col sm="6">
                        <List 
                            list={this.props.tempRequests} 
                            headers={['Kind of care', 'Start', 'End', 'Patient', 'Information', 'Status']}
                            shownEl={['type', 'startDate', 'endDate', 'name', 'info', 'status']}
                            hasEditBtn={'Apply'}
                            toggle={this.apply}
                            onClickFunction={this.showDetails}
                        />
                    </Col>
                    {/* Request Details */}
                    <Col sm="6">
                        { this.state.details.isToggled && 
                            <div className="details">
                                <Table>
                                    <thead>
                                    <tr>
                                        <th>Details</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Kind of Care</th>
                                        <td>{this.state.details.entry.type}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Start Date</th>
                                        <td>{this.state.details.entry.startDate.toString()}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">End Date</th>
                                        <td>{this.state.details.entry.endDate.toString()}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Patient</th>
                                        <td>{this.state.details.entry.name}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Information</th>
                                        <td>{this.state.details.entry.info}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Status</th>
                                        <td>{this.state.details.entry.status}</td>
                                    </tr>
                                    </tbody>
                                </Table>
                            </div>
                        }
                    </Col>
                </Row>

                {/* Modal: Apply for a request */}
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Apply for this request!</ModalHeader>
                    <ModalBody>
                        <FormC toggle={this.submitApply} formConfig={this.state.requestApplyForm}/>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

}

export default Caregiver;