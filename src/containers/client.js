import React, { Component } from 'react';
// Reacstrap
import { Button } from 'reactstrap';
// Components
import FormC from '../components/FormC';
// Constants
import { requestForm } from '../constants/formFormats';

class Client extends Component {
    constructor(props) {
        super(props);

        this.state = {
            entryForm: requestForm,
        }

        this.addRequest = this.addRequest.bind(this);
        
    }

    addRequest(content) {
        this.props.addRequestBuffer(content);
    }

    render() {
        return ( 
            <div>
               <FormC toggle={this.addRequest} formConfig={this.state.entryForm} />
            </div>
        );
    }

}

export default Client;