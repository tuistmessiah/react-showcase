import { 
    ADD_REQUEST,
} from "../constants/ActionTypes";

export const updateRequests = (payload) => {
    return {
        type: ADD_REQUEST,
        payload: payload
    }
}