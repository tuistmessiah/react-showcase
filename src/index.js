// Main
import React from 'react';
import { render } from 'react-dom';
import App from './App';
// Store
import { Provider } from 'react-redux';
import store from './store'
// CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/style.css';

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
